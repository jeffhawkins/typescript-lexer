import * as fs from 'fs';
import { Lexer }  from './Lexer';
import { EOF, ILLEGAL, IToken, nextToken} from './Tokens';

const main = () => {
  const buffer = fs.readFileSync('./samples/Calendar.tsx');
  const code = buffer.toString('utf8');

  // const tokens = [];
  // Lexer.init(code);

  // while (true) {
  //   const tok: IToken = nextToken();

  //   console.log('token: ' + JSON.stringify(tok));

  //   if (tok.type === ILLEGAL) {
  //     console.log('ILLEGAL TOKEN AT: ' + JSON.stringify(tok));
  //     break;
  //   }
  //   else if (tok.type === EOF) {
  //     console.log('done');
  //     break;
  //   }
  //   else {
  //     tokens.push(tok);
  //   }
  // }

  // console.log('token count: ' + tokens.length);

  const properties = parseProps(code);
  const interfaces = parseInterfaces(code, properties);

  let markdown = '# Calendar\n';

  markdown += '## Interfaces\n';

  for (let i = 0; i < interfaces.length; i++) {
    markdown += '```typescript\n';
    markdown += interfaces[i] + '\n';
    markdown += '```\n';
  }

  markdown += '## Properties\n';
  markdown += '|Property|Type|Required|Description|\n';
  markdown += '|---|---|---|---|\n';

  for (let i = 0; i < properties.length; i++) {
    const prop = properties[i];
    if (!prop.isEvent) {
      markdown += '|' + prop.name + '|`' + prop.type + '`|' + prop.required + '|' + prop.description + '|\n';
    }
  }

  markdown += '\n\n';
  markdown += '## Events\n';
  markdown += '|Property|Arguments|Required|Description|\n';
  markdown += '|---|---|---|---|\n';

  for (let i = 0; i < properties.length; i++) {
    const prop = properties[i];
    if (prop.isEvent) {
      markdown += '|' + prop.name + '|`' + prop.type + '`|' + prop.required + '|' + prop.description + '|\n';
    }
  }

  console.log();
  console.log(markdown);

  fs.writeFileSync('./samples/Calendar.md', markdown);
};

const parseInterfaces = (code: string, properties: any[]) => {
  const lines = code.split('\n');
  const interfaces = [];

  for (let i = 0; i < properties.length; i++) {
    const prop = properties[i];
    let definition = '';
    let type = prop.type;

    if (type.endsWith('[]')) {
      type = type.substring(0, type.length - 2);
    }
  
    if (type.startsWith('I')) {
      for (let j = 0; j < lines.length; j++) {
        let line = lines[j];

        if (line.includes('interface') && line.includes(type) && line.includes('{')) {
      
          while (true) {
            if (!line.trim().startsWith('//')) {
              definition += line + '\n';
            }

            line = lines[++j];
            if (line.includes('}')) {
              definition += line;
              break;
            }
          }

          interfaces.push(definition);
          break;
        }
      }
    }
  }

  return interfaces;
};

const parseProps = (code: string) => {
  const lines = code.split('\n');
  const properties = [];

  for (let i = 0; i < lines.length; i++) {
    if (findPropsInterface(lines[i].trim())) {
      for (let j = 1; j < lines.length; j++) {
        if (lines[i + j].trim() !== '}') {
          if (!lines[i + j].trim().startsWith('//')) {
            const props = parseProperty(lines[i + j].trim());
            if (props) {
              properties.push(props);
            }
          }
        }
        else {
          break;
        }
      }
    }
  }

  return properties;
};

const findPropsInterface = (line: string) => {
  if (!line.startsWith('//') &&
      line.includes('export') && 
      line.includes('interface') &&
      line.includes('I') &&
      line.includes('Props') &&
      line.endsWith('{')) {
    // console.log('--> ' + line);
    return true;
  }

  return false;
};

const parseProperty = (line: string) => {
  let propertyLine = line.trim();

  if (!propertyLine.length) {
    return null;
  }

  let name = propertyLine.substring(0, propertyLine.indexOf(':'));
  const optional = name.endsWith('?') ? true : false;
  
  if (optional) {
    name = name.substring(0, name.length - 1);
  }
  
  let type = propertyLine.substring(propertyLine.indexOf(':') + 1, propertyLine.indexOf(';'));
  if (type) {
    type = type.trim();
  }

  let description = '';
  if (propertyLine.includes('//')) {
    description = propertyLine.substring(propertyLine.indexOf('//') + 2, propertyLine.length).trim();
  }

  const property = {
    name: name,
    isEvent: type.startsWith('('),
    required: !optional,
    type: type,
    description: description
  };

  // console.log(JSON.stringify(property));

  return property;
};

main();