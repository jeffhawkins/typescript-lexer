
export class Lexer {
  private static input: string;
  private static position: number = 0;
  private static readPosition: number = 0;
  private static ch: string = '';

  public static init(input: string) {
    this.input = input;
    Lexer.readChar();
  }

  public static readChar = ():void => {
    if (Lexer.readPosition >= Lexer.input.length) {
      Lexer.ch = '';
    }
    else {
      Lexer.ch = Lexer.input[Lexer.readPosition];
    }

    Lexer.position = Lexer.readPosition;
    Lexer.readPosition += 1;
  };

  public static peakChar = ():string => {
    if (Lexer.readPosition >= Lexer.input.length) {
      return '';
    }
    else {
      return Lexer.input[Lexer.readPosition];
    }
  };

  public static getPosition = ():number => {
    return Lexer.position;
  };

  public static getChar = ():string => {
    return Lexer.ch;
  };

  public static getInput = (start: number, end: number):string => {
    return Lexer.input.substring(start, end);
  }

}

