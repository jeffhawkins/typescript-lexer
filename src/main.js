"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const Lexer_1 = require("./Lexer");
const Tokens_1 = require("./Tokens");
const main = () => {
    const buffer = fs.readFileSync('../../samples/Calendar.tsx');
    const code = buffer.toString('utf8');
    const tokens = [];
    const lexer = new Lexer_1.Lexer(code);
    while (true) {
        const tok = Tokens_1.nextToken(lexer);
        console.log('token: ' + JSON.stringify(tok));
        if (tok.type === Tokens_1.ILLEGAL) {
            console.log('ILLEGAL TOKEN AT: ' + JSON.stringify(tok));
            break;
        }
        else if (tok.type === Tokens_1.EOF) {
            console.log('done');
            break;
        }
        else {
            tokens.push(tok);
        }
    }
    console.log('token count: ' + tokens.length);
    console.log(' line count: ' + Tokens_1.getLineNumber());
};
main();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZVJvb3QiOiIvVXNlcnMvaGF3a2luc2ovRGV2ZWxvcG1lbnQvbWRmL21kZi1kb2N1bWVudGF0aW9uL3NyYy9sZXhlci8iLCJzb3VyY2VzIjpbIm1haW4udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSx5QkFBeUI7QUFDekIsbUNBQWdDO0FBQ2hDLHFDQUF5RTtBQUV6RSxNQUFNLElBQUksR0FBRyxHQUFHLEVBQUU7SUFDaEIsTUFBTSxNQUFNLEdBQUcsRUFBRSxDQUFDLFlBQVksQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDO0lBQzdELE1BQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7SUFFckMsTUFBTSxNQUFNLEdBQUcsRUFBRSxDQUFDO0lBQ2xCLE1BQU0sS0FBSyxHQUFVLElBQUksYUFBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBRXJDLE9BQU8sSUFBSSxFQUFFLENBQUM7UUFDWixNQUFNLEdBQUcsR0FBVyxrQkFBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRXJDLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUU3QyxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxLQUFLLGdCQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3hELEtBQUssQ0FBQztRQUNSLENBQUM7UUFDRCxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksS0FBSyxZQUFHLENBQUMsQ0FBQyxDQUFDO1lBQzFCLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDcEIsS0FBSyxDQUFDO1FBQ1IsQ0FBQztRQUNELElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNuQixDQUFDO0lBQ0gsQ0FBQztJQUVELE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUM3QyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsR0FBRyxzQkFBYSxFQUFFLENBQUMsQ0FBQztBQUNqRCxDQUFDLENBQUM7QUFFRixJQUFJLEVBQUUsQ0FBQyJ9