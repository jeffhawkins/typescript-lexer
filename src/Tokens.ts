import { Lexer } from './Lexer';

enum Tokens {
  ASSIGN = '=',
  ASTERISK = '*',
  BACK_QUOTE = '`',
  BACK_SLASH = '\\',
  BIT_AND = '&',
  BIT_OR = '|',
  COLON = ':',
  COMMA = ',',
  COMMENT = '//',
  COMMENT_BLOCK = '/*',
  DOLLAR_SIGN = '$',
  DOUBLE_EQUAL = '==',
  DOUBLE_QUOTE = '"',
  EOF = 'eof',
  EQUAL = '==',
  EQUAL_TRIPLE = '===',
  EXCLAMATION = '!',
  FORWARD_SLASH = '/',
  GREATER_THAN = '>',
  GREATER_THAN_EQUAL = '>=',
  ILLEGAL = 'illegal',
  LEFT_BRACE = '}',
  LEFT_BRACKET = '[',
  LEFT_PAREN = '(',
  LESS_THAN = '<',
  LESS_THAN_EQUAL = '<=',
  LINE_FEED = '/r',
  LOGICAL_AND = '&&',
  LOGICAL_OR = '||',
  MINUS = '-',
  NEW_LINE = '/n',
  NOT_EQUAL = '!=',
  NOT_EQUAL_TRIPLE = '!==',
  PERCENT_SIGN = '%',
  PERIOD = '.',
  PLUS = '+',
  QUESTION = '?',
  RIGHT_BRACE = '}',
  RIGHT_BRACKET = ']',
  RIGHT_PAREN = ')',
  SEMICOLON = ';',
  SINGLE_QUOTE = '\'',
  TAB = '\t'
}

export const ILLEGAL = 'illegal';
export const EOF = 'eof';

const IDENTIFIER = 'IDENTIFIER';
const KEYWORD = 'KEYWORD';
const NUMBER = 'NUMBER';

const IdentifierMap = [
  {
    literal: Tokens.LESS_THAN_EQUAL,
    token: 'LESS_THAN_EQUAL'
  },
  {
    literal: Tokens.GREATER_THAN_EQUAL,
    token: 'GREATER_THAN_EQUAL'
  },
  {
    literal: Tokens.LOGICAL_AND,
    token: 'LOGICAL_AND'
  },
  {
    literal: Tokens.LOGICAL_OR,
    token: 'LOGICAL_OR'
  },
  {
    literal: Tokens.COMMENT_BLOCK,
    token: 'COMMENT_BLOCK'
  },
  {
    literal: Tokens.COMMENT,
    token: 'COMMENT'
  },
  {
    literal: Tokens.EQUAL,
    token: 'EQUAL'
  },
  {
    literal: Tokens.NOT_EQUAL,
    token: 'NOT_EQUAL'
  },
  {
    literal: Tokens.NOT_EQUAL_TRIPLE,
    token: 'NOT_EQUAL_TRIPLE'
  },
  {
    literal: Tokens.EQUAL_TRIPLE,
    token: 'EQUAL_TRIPLE'
  },
  {
    literal: Tokens.TAB,
    token: 'TAB'
  },
  {
    literal: Tokens.NEW_LINE,
    token: 'NEW_LINE'
  },
  {
    literal: Tokens.LINE_FEED,
    token: 'LINE_FEED'
  },
  {
    literal: Tokens.COMMA,
    token: 'COMMA'
  },
  {
    literal: Tokens.SEMICOLON,
    token: 'SEMICOLON'
  },
  {
    literal: Tokens.LEFT_PAREN,
    token: 'LEFT_PAREN'
  },
  {
    literal: Tokens.RIGHT_PAREN,
    token: 'RIGHT_PAREN'
  },
  {
    literal: Tokens.RIGHT_BRACE,
    token: 'RIGHT_BRACE'
  },
  {
    literal: Tokens.LEFT_BRACE,
    token: 'LEFT_BRACE'
  },
  {
    literal: Tokens.ASSIGN,
    token: 'ASSIGN'
  },
  {
    literal: Tokens.ASTERISK,
    token: 'ASTERISK'
  },
  {
    literal: Tokens.COMMA,
    token: 'COMMA'
  },
  {
    literal: Tokens.SINGLE_QUOTE,
    token: 'SINGLE_QUOTE',
  },
  {
    literal: Tokens.DOUBLE_QUOTE,
    token: 'DOUBLE_QUOTE'
  },
  {
    literal: Tokens.MINUS,
    token: 'MINUS'
  },
  {
    literal: Tokens.PLUS,
    token: 'PLUS'
  },
  {
    literal: Tokens.PERIOD,
    token: 'PERIOD'
  },
  {
    literal: Tokens.FORWARD_SLASH,
    token: 'FORWARD_SLASH'
  },
  {
    literal: Tokens.COLON,
    token: 'COLON'
  },
  {
    literal: Tokens.QUESTION,
    token: 'QUESTION'
  },
  {
    literal: Tokens.GREATER_THAN,
    token: 'GREATER_THAN'
  },
  {
    literal: Tokens.LESS_THAN,
    token: 'LESS_THAN'
  },
  {
    literal: Tokens.LEFT_BRACKET,
    token: 'LEFT_BRACKET'
  },
  {
    literal: Tokens.RIGHT_BRACKET,
    token: 'RIGHT_BRACKET'
  },
  {
    literal: Tokens.BIT_OR,
    token: 'BIT_OR'
  },
  {
    literal: Tokens.BIT_AND,
    token: 'BIT_AND'
  },
  {
    literal: Tokens.EXCLAMATION,
    token: 'EXCLAMATION'
  },
  {
    literal: Tokens.BACK_QUOTE,
    token: 'BACK_QUOTE'
  },
  {
    literal: Tokens.DOLLAR_SIGN,
    token: 'DOLLAR_SIGN'
  },
  {
    literal: Tokens.PERCENT_SIGN,
    token: 'PERCENT_SIGN'
  }
];

const Keywords = [
  'any',
  'as',
  'async',
  'await',
  'boolean',
  'break',
  'catch',
  'class',
  'const',
  'constructor',
  'continue',
  'debugger',
  'declare',
  'default',
  'delete',
  'do',
  'else',
  'enum',
  'export',
  'export',
  'extends',
  'false',
  'finally',
  'for',
  'from',
  'from',
  'function',
  'get',
  'if',
  'implements',
  'import',
  'in',
  'instanceof',
  'interface',
  'let',
  'module',
  'namespace',
  'new',
  'null',
  'number',
  'of',
  'package',
  'private',
  'protected',
  'public',
  'require',
  'return',
  'set',
  'static',
  'string',
  'super',
  'switch',
  'symbol',
  'this',
  'throw',
  'true',
  'try',
  'type',
  'typeof',
  'undefined',
  'var',
  'void',
  'while',
  'with',
  'yield'
];

export interface IToken {
  type: string;
  literal: string;
}

export const nextToken = ():IToken => {
  let tok: IToken = { type: ILLEGAL, literal: '' };

  skipWhiteSpace();

  switch (Lexer.getChar()) {

    case '=':
      if (Lexer.peakChar() === '=') {
        Lexer.readChar();

        if (Lexer.peakChar() === '=') {
          Lexer.readChar();
          tok = newToken(Tokens.EQUAL_TRIPLE, "===");
        }
        else {
          tok = newToken(Tokens.EQUAL, "==");
        }
      }
      else {
        tok = newToken(Tokens.ASSIGN, Lexer.getChar());
      }
      break;

    case '!':
      if (Lexer.peakChar() === '=') {
        Lexer.readChar();

        if (Lexer.peakChar() === '=') {
          Lexer.readChar();
          tok = newToken(Tokens.NOT_EQUAL_TRIPLE, "!==");
        }
        else {
          tok = newToken(Tokens.NOT_EQUAL, "!=");
        }
      }
      else {
        tok = newToken(Tokens.EXCLAMATION, Lexer.getChar());
      }
      break;

    case ';':
      tok = newToken(Tokens.SEMICOLON, Lexer.getChar());
      break;

    case '(':
      tok = newToken(Tokens.LEFT_PAREN, Lexer.getChar());
      break;

    case ')':
      tok = newToken(Tokens.RIGHT_PAREN, Lexer.getChar());
      break;

    case ',':
      tok = newToken(Tokens.COMMA, Lexer.getChar());
      break;

    case '':
      tok = newToken(Tokens.EOF, '');
      break;

    case '*':
      tok = newToken(Tokens.ASTERISK, Lexer.getChar());
      break;

    case '\'':
      tok = newToken(Tokens.SINGLE_QUOTE, Lexer.getChar());
      break;

    case '{':
      tok = newToken(Tokens.LEFT_BRACE, Lexer.getChar());
      break;

    case '}':
      tok = newToken(Tokens.RIGHT_BRACE, Lexer.getChar());
      break;

    case '-':
      tok = newToken(Tokens.MINUS, Lexer.getChar());
      break;

    case '+':
      tok = newToken(Tokens.PLUS, Lexer.getChar());
      break;

    case '.':
      tok = newToken(Tokens.PERIOD, Lexer.getChar());
      break;

    case '/':
      if (Lexer.peakChar() === '/') {
        Lexer.readChar();
        Lexer.readChar();

        skipWhiteSpace();

        const comment = readLineComment();
        tok = newToken(Tokens.COMMENT, comment);
      }
      else if (Lexer.peakChar() === '*') {
        Lexer.readChar();
        Lexer.readChar();
        skipWhiteSpace();

        const comment = readBlockComment();
        tok = newToken(Tokens.COMMENT_BLOCK, comment);
      }
      else {
        tok = newToken(Tokens.FORWARD_SLASH, Lexer.getChar());
      }
      break;

    case ':':
      tok = newToken(Tokens.COLON, Lexer.getChar());
      break;

    case '?':
      tok = newToken(Tokens.QUESTION, Lexer.getChar());
      break;

    case '>':
      if (Lexer.peakChar() === '=') {
        Lexer.readChar();
        tok = newToken(Tokens.GREATER_THAN_EQUAL, '>=');
      }
      else {
        tok = newToken(Tokens.GREATER_THAN, Lexer.getChar());
      }
      break;

    case '<':
      if (Lexer.peakChar() === '=') {
        Lexer.readChar();
        tok = newToken(Tokens.LESS_THAN_EQUAL, '<=');
      }
      else {
        tok = newToken(Tokens.LESS_THAN, Lexer.getChar());
      }
      break;

    case '[':
      tok = newToken(Tokens.LEFT_BRACKET, Lexer.getChar());
      break;

    case ']':
      tok = newToken(Tokens.RIGHT_BRACKET, Lexer.getChar());
      break;

    case '|':
      if (Lexer.peakChar() === '|') {
        Lexer.readChar();
        tok = newToken(Tokens.LOGICAL_OR, '||');
      }
      else {
        tok = newToken(Tokens.BIT_OR, Lexer.getChar());
      }
      break;

    case '&':
      if (Lexer.peakChar() === '&') {
        Lexer.readChar();
        tok = newToken(Tokens.LOGICAL_AND, '&&');
      }
      else {
        tok = newToken(Tokens.BIT_AND, Lexer.getChar());
      }
      break;

    case '`':
      tok = newToken(Tokens.BACK_QUOTE, Lexer.getChar());
      break;

    case '$':
      tok = newToken(Tokens.DOLLAR_SIGN, Lexer.getChar());
      break;

    case '"':
      tok = newToken(Tokens.DOUBLE_QUOTE, Lexer.getChar());
      break;

    case '%':
      tok = newToken(Tokens.PERCENT_SIGN, Lexer.getChar());
      break;

    case '\n':
      tok = newToken(Tokens.NEW_LINE, Lexer.getChar());
      break;

    case '\r':
      tok = newToken(Tokens.LINE_FEED, Lexer.getChar());
      break;

    case '\t':
      tok = newToken(Tokens.TAB, Lexer.getChar());
      break;

    default: {
      if (isLetter(Lexer.getChar())) {
        let type = IDENTIFIER;
        const identifier = readIdentifier();
        if (isKeyword(identifier)) {
          type = KEYWORD;
        }

        return {
          type: type,
          literal: identifier
        };
      }
      else if (isDigit(Lexer.getChar())) {
        return {
          type: NUMBER,
          literal: readNumber()
        }
      }
      else {
        tok = newToken(Tokens.ILLEGAL, Lexer.getChar());
      }
    }
  }

  Lexer.readChar();

  return tok;
};

const readIdentifier = (): string => {
  let position: number = Lexer.getPosition();

  while (isLetter(Lexer.getChar())) {
    Lexer.readChar();
  }

  return Lexer.getInput(position, Lexer.getPosition());
};

const readNumber = (): string => {
  const position: number = Lexer.getPosition();

  while (isDigit(Lexer.getChar())) {
    Lexer.readChar();
  }

  return Lexer.getInput(position, Lexer.getPosition());
};

const readLineComment = (): string => {
  const position: number = Lexer.getPosition();

  while (Lexer.getChar() !== '\n' && Lexer.getChar() !== '\r') {
    Lexer.readChar();
  }

  return Lexer.getInput(position, Lexer.getPosition());
};

const readBlockComment = (): string => {
  const position: number = Lexer.getPosition();

  while (Lexer.getChar() !== '*' && Lexer.peakChar() !== '/') {
    Lexer.readChar();
  }

  return Lexer.getInput(position, Lexer.getPosition());
};

const isLetter = (ch: string): boolean => {
  return 'a' <= ch && ch <= 'z' || 'A' <= ch && ch <= 'Z' || ch === '_';
};

const newToken = (type: Tokens, ch: string): IToken => {
  return {
    type: lookupIdentifier(type),
    literal: ch
  };
};

const skipWhiteSpace = ():void => {
  while (Lexer.getChar() === ' ' || Lexer.getChar() === '\t' || Lexer.getChar() === '\n' || Lexer.getChar() === '\r') {
    Lexer.readChar();
  }
};

const isDigit = (ch: string):boolean => {
  return '0' <= ch && ch <= '9';
};

const lookupIdentifier = (literal: Tokens):string => {
  for (let i = 0; i < IdentifierMap.length; i++) {
    const ident = IdentifierMap[i];
    if (ident.literal === literal) {
      return ident.token;
    }
  }

  return literal;
};

const isKeyword = (literal: string):boolean => {
  for (const k of Keywords) {
    if (literal === k) {
      return true;
    }
  }

  return false;
};